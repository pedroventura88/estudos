package br.com.devmedia.TrabalhandoComClassesJava;

import javax.swing.JOptionPane;

public class ClasseString {

	public static void main(String[] args) {
		String nome = JOptionPane.showInputDialog(null, "Informe o nome.");
		int idade = Integer.parseInt(JOptionPane.showInputDialog(null, "informe a idade"));
		String sobrenome = "Meu sobrenome";
		String nomeFormal = new String ("Maneira formal de se criar um objeto do tipo String.");
		System.out.println(">> "+nome.concat(" "+nomeFormal));
		JOptionPane.showMessageDialog(null, "Nome e Sobrenome: " + nome +" "+ sobrenome);
	}

}
