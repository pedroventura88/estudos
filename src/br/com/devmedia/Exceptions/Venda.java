package br.com.devmedia.Exceptions;

import java.util.ArrayList;
import java.util.List;

public class Venda {
	
	List<ItemVenda> itens = new ArrayList<>();
	
	public void adicionar (ItemVenda itemVenda) {
		if (itemVenda == null) {
			throw new VendaException("Item recebido n�o pode ser nulo", 500);
		}
		itens.add(itemVenda);
	}
	
	public double getTotal () {
		
		double total = 0;
		
		for (ItemVenda itemVenda : itens) {
			total += itemVenda.getPreco();
		}
		
		return total;
	}

}
