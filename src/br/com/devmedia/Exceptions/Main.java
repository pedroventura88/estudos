package br.com.devmedia.Exceptions;

public class Main {

	public static void main(String[] args) {
		
		try {
			ItemVenda camisa = new ItemVenda();
			camisa.setDescricao("Camisa de manga comprida");
			camisa.setPreco(55);
			
			ItemVenda bermuda = new ItemVenda();
			bermuda.setDescricao("Bermuda Preta");
			bermuda.setPreco(35);
			
			Venda venda = new Venda();
			venda.adicionar(camisa);
			venda.adicionar(bermuda);
//			venda.adicionar(null);
			
			System.out.println("Total em Vendas: " + venda.getTotal());
		} catch (VendaException e) {
			System.out.println("Erro ao processar Total: "+ e.getMessage() + " - C�digo: "+e.getCodigo());
		} catch (RuntimeException e) { //Capturar exce��es da mais espec�fica, p/ a mais Abrangente. NullPointer, Herda de RuntimeException.
			System.out.println("Erro em tempo de compila��o Real.");
		} finally { //bloco ser� executado, independentemente se foi com sucesso ou n�o. Aqui por exemplo, pode-se fechar uma conex�o com o banco.
			System.out.println("Processo Encerrado.");
		}
		
	}

}
