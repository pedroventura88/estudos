package br.com.devmedia.Exceptions;

public class VendaException extends RuntimeException{
	
	private int codigo;
	
	public VendaException (String mensagem, int codigo) {
		super(mensagem);
		
		this.codigo = codigo;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
}
