package br.com.devmedia.OrientacaoObj;

import java.util.Scanner;

public class UsandoWhile {

	public static void main(String[] args) {
		//ENTRADA
		Scanner entrada = new Scanner(System.in);
		System.out.println("Digite um numero:");
		int condicao = entrada.nextInt();
		//PROCESSAMENTO
		//O TESTE DO WHILE � EXECUTADO ANTES DA EXECU��O DA LINHA... Se O TESTE precisar ser executado DEPOIS, utiliza-se o DO antes
		while (condicao != 1) {
			System.out.println("Voc� digitou o numero: " + condicao);
			System.out.println("Digite 1 para Sair do programa.");
			condicao = entrada.nextInt();
		}
		
		System.out.println("Voc� saiu do Programa! At� a pr�xima!");
	}

}
