package br.com.devmedia.OrientacaoObj;

import javax.swing.JOptionPane;

public class UsandoJoptionPaneWrapper {

	public static void main(String[] args) {
		
		//entrada
		String nome = 	JOptionPane.showInputDialog(null, "Digite seu nome");
		String sobrenome = 	JOptionPane.showInputDialog(null, "Digite seu sobrenome");
		String salarioAux = JOptionPane.showInputDialog(null, "Digite seu salario");
		String idadeAux = JOptionPane.showInputDialog(null, "DIgite sua idade");
		
		//processamento
		String nomeCompleto = nome +" "+ sobrenome;
		JOptionPane.showMessageDialog(null, nomeCompleto);
		double salario = Double.parseDouble(salarioAux);
		int idade = Integer.parseInt(idadeAux);
		double resultado = salario * idade;
		JOptionPane.showMessageDialog(null, "Salario x Idade:" + resultado);
	
	}

}
