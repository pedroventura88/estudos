package br.com.devmedia.OrientacaoObj;

import javax.swing.JOptionPane;

public class ManipulandoArrays {

	public static void main(String[] args) {
		
		String idadeAux = JOptionPane.showInputDialog(null, "Informe a quantidade de idades a serem percorridas:");
		int qtdIdade = Integer.parseInt(idadeAux);
		
		int[] idadeArray = new int[qtdIdade];
		
		for (int i = 0; i < idadeArray.length; i++) {
			idadeArray[i] = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite a idade: " + (i+1)));
		}
		
		int somaIdade = 0;
		for (int i = 0; i < idadeArray.length; i++) {
			somaIdade += idadeArray[i];
		}
	
		JOptionPane.showMessageDialog(null, "Media de idade �: " + somaIdade / qtdIdade);
	
	}

}
