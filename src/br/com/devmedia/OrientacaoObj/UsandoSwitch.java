package br.com.devmedia.OrientacaoObj;

import java.util.Scanner;

public class UsandoSwitch {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int condicao = 0;
		while (condicao == 0) {
			//entrada
			Scanner entrada = new Scanner(System.in);
			System.out.println("Digite um numero de 0 a 9:");
			int num = entrada.nextInt();
			String numExtenso = "";
			
			//processamento - Obs.: Switch s� funciona para numero INTEIRO ou CHAR
			switch (num) {
			case 0:
				numExtenso = "zero";
				break;
			case 1:
				numExtenso = "um";
				break;
			case 2:
				numExtenso = "dois";
				break;
			case 3:
				numExtenso = "tres";
				break;
			case 4:
				numExtenso = "quatro";
				break;
			case 5:
				numExtenso = "cinco";
				break;
			case 6:
				numExtenso = "seis";
				break;
			case 7:
				numExtenso = "sete";
				break;
			case 8:
				numExtenso = "oito";
				break;
			case 9:
				numExtenso = "nove";
				break;
			default:
				numExtenso = "Valor n�o suportado";
				break;
			}
			
			System.out.println("Numero extenso: "+numExtenso);
			System.out.println("Digite 1 para sair ou 0 para continuar.");
			condicao = sc.nextInt();
		}
		System.out.println("Voc� saiu do Sistema!!!");
	}

}
