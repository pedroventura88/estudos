package br.com.devmedia.OrientacaoObj;

import java.util.Scanner;

public class TabuadaComFor {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe o numero para realizar a tabuada:");
		int num = sc.nextInt();
		
		for (int i = 1; i <= 10; i++) {
			System.out.println(num + " x " + i + " = " + i*num);
		}
	}

}
